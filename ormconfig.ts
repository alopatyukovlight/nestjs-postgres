import * as dotenv from 'dotenv';

dotenv.config({path: './.env'});


module.exports = {
  type: 'postgres',
  host: 'localhost',
  port: Number.parseInt(process.env.DB_PORT, 10),
  username: process.env.DB_USER,
  password: process.env.DB_PASSWORD,
  database: process.env.DB_NAME,
  entities: [ './src/**/*.entity.ts' ],
  synchronize: true,
  migrationsTableName: 'migrations',
  migrations: [ './migration/*.ts' ],
  cli: {
    migrationsDir: './migration'
  },
  logging: true,
};