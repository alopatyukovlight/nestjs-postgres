import { Exclude, Transform } from "class-transformer";
import * as bcrypt from 'bcryptjs';
import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class User {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  username: string;

  @Exclude({toPlainOnly: true})
  @Transform(value => bcrypt.hashSync(value, 10))
  @Column()
  password: string;
}