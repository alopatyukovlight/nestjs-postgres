import { Body, ClassSerializerInterceptor, Controller, Post, UseInterceptors } from '@nestjs/common';
import { AuthService } from "./auth.service";

@UseInterceptors(ClassSerializerInterceptor)
@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {
  }

  @Post("create")
  public async create() {
    return await this.authService.create()
  }

  @Post("signin")
  public async signIn(@Body() credentional: { username: string, password: string }) {
    return await this.authService.auth(credentional);
  }
}
