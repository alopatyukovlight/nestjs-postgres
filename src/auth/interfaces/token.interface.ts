export interface IToken {
  createTokenDate: number,
  expiresTokenDate: number,
  accessToken: string
}