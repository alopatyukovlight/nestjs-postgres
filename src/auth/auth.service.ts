import { HttpException, HttpStatus, Inject, Injectable } from "@nestjs/common";
import { classToPlain, plainToClass } from "class-transformer";
import { User } from "./entities/user.entity";
import { Repository } from "typeorm";

import { IToken } from "./interfaces/token.interface";

import * as jwt from 'jsonwebtoken';
import * as bcrypt from 'bcryptjs';

@Injectable()
export class AuthService {
  constructor(@Inject('UserRepositoryToken') private readonly usersRepository: Repository<User>) {
  }

  async validateUser(username: string) {
    return await this.usersRepository.findOne({username});
  }

  async create() {
    let json = {
      username: "art",
      password: "qwerty"
    };
    let user = plainToClass(User, json);
    return await this.usersRepository.save(user);
  }

  async auth(cred: { username: string; password: string }): Promise<{ Token: IToken, User: any }> {
    console.log(cred);
    let user = await this.usersRepository.findOne({username: cred.username});
    if (!user) {
      throw new HttpException(`User with name ${cred.username} is not exist`, HttpStatus.NOT_FOUND);
    }
    if (await bcrypt.compare(cred.password, user.password)) {
      return {
        Token: this.createToken(user.username),
        User: classToPlain(user)
      }
    } else {
      throw new HttpException('Password or username is not validate', HttpStatus.FORBIDDEN);
    }
  }

  public createToken(username: string): IToken {
    const expiresIn = 3600;
    const createTokenDate: number = new Date().getTime();
    const expiresTokenDate: number = createTokenDate + expiresIn;
    const accessToken = jwt.sign({username}, process.env.SECRET_KEY, {expiresIn});
    return {
      createTokenDate,
      expiresTokenDate,
      accessToken
    };
  }
}