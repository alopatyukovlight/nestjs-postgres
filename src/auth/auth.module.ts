import { Module } from '@nestjs/common';
import { AuthController } from './auth.controller';
import { AuthService } from "./auth.service";
import { DatabaseModule } from "../core/database/database.module";
import { userProviders } from "./entities/user.providers";

@Module({
  controllers: [ AuthController ],
  providers: [ ...userProviders, AuthService ],
  imports: [ DatabaseModule ]
})
export class AuthModule {
}
