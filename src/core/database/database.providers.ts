import { ConnectionOptions, createConnection } from 'typeorm';

export const typeOrmConfig = {
  type: 'postgres',
  host: process.env.DB_HOST,
  port: Number.parseInt(process.env.DB_PORT, 10),
  username: process.env.DB_USER,
  password: process.env.DB_PASSWORD,
  database: process.env.DB_NAME,
  entities: [ './src/**/*.entity.ts' ],
  synchronize: true,
  migrationsTableName: 'migrations',
  migrations: [ './migration/*.ts' ],
  cli: {
    migrationsDir: './migration'
  },
  logging: true,
};

export const databaseProviders = [
  {
    provide: 'DbConnectionToken',
    useFactory: async () => await createConnection(<ConnectionOptions>typeOrmConfig)
  },
];
